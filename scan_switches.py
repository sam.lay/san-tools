from getpass import getpass as getpass
import re
import paramiko
from pprint import pprint
import os
import yaml

filepath = './example.yml'
outfilepath = './san_switch_cmdb_data.csv'

brocade_model_map = { '165': 'X6-4',
                      '166': 'X6-8',
                      '162': 'G620',
                      '183': 'G620',
                      '170': 'G610',
                      '173': 'G630',
                      '184': 'G630',
                      '178': '7810',
                      '179': 'X7-4',
                      '180': 'X7-8',
                      '181': 'G720',
                      '189': 'G730' }
   

def read_yaml(file_path):
    with open(file_path, 'r') as file:
        try:
            yaml_content = yaml.safe_load(file)
        except Exception as err:
            print('*'*25, '\n')
            print(err)
            print('*'*25, '\n')
    return yaml_content

def cisco_get_cmdb_data(hostname, username, password, sshport=22):
    name = wwn  = manufacturer = model_id = serial_number = location = False
    san = firmware = model_number = desc_line_next = description = False
    manufacturer = 'Cisco'
    
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname, sshport, username, password)

        cmd = 'show hardware'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if line == '':
                continue
            if fields[0] == 'system:' and firmware == False:
                firmware = fields[-1]
            elif fields[0] == 'Hardware' and description == False:
                desc_line_next = True
            elif desc_line_next == True:
                description = line
                desc_line_next = False
            elif re.match('^Model number is', line) and model_number == False:
                model_number = fields[-1]
            elif re.match('^Serial number is', line) and serial_number == False:
                serial_number = fields[-1]
            elif re.match('^Device name:', line) and name == False:
                name = fields[-1]

        cmd = 'show wwn switch'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if re.match('^Switch WWN.*', line):
                wwn = fields[-1]
        stdout.close()
        ssh.close()
       
        print('{},{},{},{},{},{},{},{}'.format(name, wwn, manufacturer, \
                                               model_number, description, \
                                               serial_number, ip, firmware))
    except Exception as err:
        print(repr(err))
    ssh = None

def brocade_get_cmdb_data(hostname, username, password, sshport=22):
    manufacturer = 'Brocade'
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(hostname, sshport, username, password)
        """ Run the 'switchshow' command to get name and model ID """
        cmd = 'switchshow'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            fields = line.split()
            if fields[0] == 'switchName:':
                name = fields[1]
            elif fields[0] == 'switchType:':
                switch_type = fields[1][0:3]
                model_number = brocade_model_map[switch_type]
            else:
                continue
        stdout.close
        """ run the 'firmwareshow' command to get the firware version"""
        cmd = 'firmwareshow'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            line = line.strip()
            if re.match('^FOS.*', line):
                tag, firmware = line.split()
        stdout.close
        """ Run 'wwn -sn' command to get the wwnn and serial number of the switch"""
        cmd = 'wwn -sn'
        stdin, stdout, stderr = ssh.exec_command(cmd)
        stdin.close()
        stderr.close()
        for line in stdout.readlines():
            if re.match('^WWN: ', line):
                wwn = line.split()[1]
            elif re.match('^SN: ', line):
                serial_number = line.split()[1]

        description = False
        print('{},{},{},{},{},{},{},{}'.format(name, wwn, manufacturer, \
                                               model_number, description, \
                                               serial_number, ip, firmware))
        stdout.close
        ssh.close()
    except Exception as err:
        print(repr(err))

# main

username = input('User name:  ')
password = getpass('Password: ')

ip_lists = read_yaml(filepath)

print('Name,WWNN,Manufacturer,Model ID,Serial number,Location,SAN,ip_address,firmware')

for ip in ip_lists['cisco_ips']:
    if ip in ip_lists['skip_ips']:
        continue
    cisco_get_cmdb_data(ip, username, password)

for ip in ip_lists['brocade_ips']:
    if ip in ip_lists['skip_ips']:
        continue
    brocade_get_cmdb_data(ip, username, password)
