python3 -m venv venv
. ./venv/bin/activate
pip install wheel
pip install --upgrade pip
pip install -f requirements.txt
echo "run '. ./venv/bin/activate' to activate the virtual environment"
