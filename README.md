# SAN Tools Project

Just a small of examples for querying SAN switches and arrays to
collect data for reporting, alerting and trending.

CLone the repository
cd into the project directory
run the setup.py shell script to create a virtual environment

The scan_switches reads IP addresses for Cisco and Brocade switches
from the examples.yml file.  It also has an exclusion list for IPs
that do not respond.
